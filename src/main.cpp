#include <windows.h>

#include "TSHooks.hpp"

ADDR gDemoGame__mainJZLoc;
#ifdef NOSLEEP_DEDI
ADDR gDemoGame__mainSleep1Loc;
#endif

bool init()
{
	BlInit;

	BlScanHex(gDemoGame__mainJZLoc, "74 34 80 3D ? ? ? ? ?");
	BlPatchByte(gDemoGame__mainJZLoc, 0xEB);

#ifdef NOSLEEP_DEDI
	BlScanHex(gDemoGame__mainSleep1Loc, "75 08 6A 01");
	gDemoGame__mainSleep1Loc += 3;
	BlPatchByte(gDemoGame__mainSleep1Loc, 0x00);
#endif

	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");
	return true;
}

bool deinit()
{
	BlPatchByte(gDemoGame__mainJZLoc, 0x74);
#ifdef NOSLEEP_DEDI
	BlPatchByte(gDemoGame__mainSleep1Loc, 0x01);
#endif

	BlPrintf("%s: deinit'd", PROJECT_NAME);
	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch (reason) {
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
