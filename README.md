# No Sleep
This DLL prevents the game from running slower in the background.

## Usage
Use the latest version of [RedBlocklandLoader](https://gitlab.com/Eagle517/redblocklandloader/-/releases) to easily load the DLL into the game.

## Building
This DLL relies on [TSFuncs](https://gitlab.com/Eagle517/tsfuncs) which you will need to build first.  Building with CMake is straightforward, although it has only been setup for MinGW.  A toolchain has been included for cross-compiling on Linux.

Compile with `NOSLEEP_DEDI` defined to enable an additional patch which prevents dedicated servers from sleeping for 1ms every tick.  This patch is disabled by default because it causes dedicated servers to become very CPU intensive.
